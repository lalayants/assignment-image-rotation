#include "../include/flipper.h"
#include "bmp_file.h"
#include "file_opener.h"

static uint8_t clean_up(FILE **f, struct image* old, struct image* new){
    if (f)
        close_file(f);

    if (old && old->width != 0)
        image_free(old);

    if (new && new->width != 0)
        image_free(new);

    return 1;
}

int main( int argc, char** argv ) {

    (void) argc; (void) argv; // suppress 'unused parameters' warning
    if (argc != 3) {
        printf("Error! You have to enter 2 file paths (input and output)\n");
        return 1;
    }

    //create paths variables
    const char *input_path = argv[1];
    const char *output_path = argv[2];

    //read file
    FILE *fp;
    if (open_file(input_path, "rb", &fp) != FILE_OPENED_SUCCESSFULLY)
        return clean_up(&fp, NULL, NULL);

    //read image from file
    struct image image = {0};
    if (from_bmp(fp, &image) != READ_OK)
        return clean_up(&fp, &image, NULL);

    //flip image
    struct image new =image_flip_anticlockwise_90(&image);

    //open new file
    if (open_file(output_path, "wb", &fp) != FILE_OPENED_SUCCESSFULLY)
        return clean_up(&fp, &image, &new);

    //save to new file
    if (to_bmp(fp, &new) != WRITE_OK)
        return clean_up(&fp, &image, &new);

    //clean up
    clean_up(&fp, &image, &new);

    return 0;
}
