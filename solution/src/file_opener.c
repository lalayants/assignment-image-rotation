//
// Created by Kirill Lalayants on 10/9/22.
//

#include "../include/file_opener.h"

//output lines for file_open_status
static const char *file_open_status_verbose_lines[] = {
        [FILE_OPENED_SUCCESSFULLY] = "File opened\n",
        [FILE_OPENED_UNSUCCESSFULLY] = "Wrong path\n"
};

//output lines for file_close_status
static const char *file_close_status_verbose_lines[] = {
        [FILE_CLOSED_SUCCESSFULLY] = "Ffile closed\n",
        [FILE_CLOSED_UNSUCCESSFULLY] = "Error closing file\n"
};

//verbose file_open_status and return
static enum file_open_status file_open_status_verbose(enum file_open_status status){
    printf("%s",file_open_status_verbose_lines[status]);
    return status;
}

//verbose file_close_status and return
static enum file_close_status file_close_status_verbose(enum file_close_status status){
    printf("%s",file_close_status_verbose_lines[status]);
    return status;
}

//opens file and returns file_open_status
enum file_open_status open_file(char const *filename, char const *mode, FILE **f){
    if ((*f = fopen(filename, mode)) != NULL)
        return file_open_status_verbose((enum file_open_status) FILE_OPENED_SUCCESSFULLY);
    return file_open_status_verbose((enum file_open_status) FILE_OPENED_UNSUCCESSFULLY);
}

//opens file and returns file_close_status
enum file_close_status close_file(FILE **f){
    if (!fclose(*f))
        return file_close_status_verbose((enum file_close_status) FILE_CLOSED_SUCCESSFULLY);
    return file_close_status_verbose((enum file_close_status) FILE_CLOSED_UNSUCCESSFULLY);
}
