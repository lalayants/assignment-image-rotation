//
// Created by Kirill Lalayants on 10/10/22.
//

#include "../include/image.h"

//set sizes of image
static void image_set_size(struct image * image, uint64_t w, uint64_t h){
    image->height = h;
    image->width = w;
}

//malloc memory for pixels in heap
static void image_malloc_data(struct image * image){
    image->data = malloc(sizeof(struct pixel) * image->height * image->width);
}

//image constructor
struct image image_create(uint64_t w, uint64_t h){
    struct image image;
    image_set_size(&image,  w,  h);
    image_malloc_data(&image);
    return image;
}

//image cleaner
void image_free(struct image * image){
    free(image->data);
}
