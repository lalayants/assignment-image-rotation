//
// Created by Kirill Lalayants on 10/9/22.
//

#include "../include/bmp_file.h"

//output lines for bmp_read_status
static const char* const bmp_read_status_messages_lines[] = {
        [READ_OK] = "Picture was \n",
        [READ_INVALID_SIGNATURE] = "Error, unknown signature\n",
        [READ_INVALID_BITS] = "Error, wrong bites\n",
        [READ_INVALID_HEADER] = "Error, wrong header\n",
};

//output lines for bmp_write_status
static const char* const bmp_write_status_messages_lines[] = {
        [WRITE_OK] = "File saved\n",
        [WRITE_ERROR] = "Error, file was not saved\n",
};

//verbose bmp_read_status and return
static enum bmp_read_status bmp_read_status_verbose(enum bmp_read_status status){
    fprintf(stderr, "%s",bmp_read_status_messages_lines[status]);
    return status;
}
//verbose bmp_write_status and return
static enum bmp_write_status bmp_write_status_verbose(enum bmp_write_status status){
    fprintf(stderr,"%s",bmp_write_status_messages_lines[status]);
    return status;
}

//calculate padding for image
static uint16_t get_padding(const struct image* img){
    if (img->width*3 % 4 == 0)
        return 0;
    return 4 - img->width*3 % 4;
}

//header constructor
static struct bmp_header new_header(struct image const* img) {
    struct bmp_header new_header = {
            .bfType = 0x4D42, // hex description of bmp file from wikipedia
            // weight * height * size of 1 pixel + height * amount of padding-bytes + signature
            .bfileSize = (img -> width) * (img -> height) * sizeof(struct pixel) + (img -> height) *
                                                                                   get_padding(img) + sizeof(struct bmp_header),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header), // size of signature
            .biSize = 40,
            .biWidth = img -> width,
            .biHeight = img -> height,
            .biPlanes = 1,
            .biBitCount = 24, // only 24-bit bmp files in solution
            .biCompression = 0,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0};
    // size of (all file size - size of signature)
    new_header.biSizeImage = new_header.bfileSize - new_header.bOffBits; // size of "clear image"
    return new_header;
}

//reads struct image from bmp FILE
enum bmp_read_status from_bmp( FILE* in, struct image* img ){
    //read header
    struct bmp_header header;
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1){
        return bmp_read_status_verbose((enum bmp_read_status) READ_INVALID_HEADER);
    }

    //prepare image
    struct image temp = image_create(header.biWidth, header.biHeight);
    uint16_t paddings_number = get_padding(&temp);

    //read image
    for (uint64_t i = 0; i < temp.height; i++) {
        if (fread(&((temp.data)[i * temp.width]), sizeof(struct pixel), temp.width, in)){
            if (fseek(in, paddings_number, SEEK_CUR)){
                image_free(&temp);
                return bmp_read_status_verbose((enum bmp_read_status) READ_INVALID_BITS);
            }
        } else {
            image_free(&temp);
            return bmp_read_status_verbose((enum bmp_read_status) READ_INVALID_SIGNATURE);
        }
    }

    // for Database transaction
    *img = temp;

    return bmp_read_status_verbose((enum bmp_read_status) READ_OK);
}

//writes struct image from bmp FILE
enum bmp_write_status to_bmp( FILE* out, struct image const* img ){
    //preparation
    uint16_t paddings_number = get_padding(img);
    const char forPadding[] = {0, 0, 0};

    //writing header
    struct bmp_header bmpHeader = new_header(img);
    if (fwrite(&bmpHeader, sizeof(struct bmp_header), 1, out) != 1)
        return bmp_write_status_verbose((enum bmp_write_status) WRITE_ERROR);

    //writing image
    for (uint64_t i = 0; i < img->height; i++) {
        if (fwrite(&(img->data[i * img->width]), sizeof(struct pixel), img->width, out) != img->width || fwrite(&forPadding, 1, paddings_number, out) != paddings_number)
            return bmp_write_status_verbose((enum bmp_write_status) WRITE_ERROR);
    }

    return bmp_write_status_verbose((enum bmp_write_status) WRITE_OK);
}
