//
// Created by Kirill Lalayants on 10/10/22.
//

#include "../include/flipper.h"

//flips image 90 degrees anticlockwise
struct image image_flip_anticlockwise_90(struct image* image) {
    struct image temp = image_create(image->height, image->width);
    for (uint64_t i = 0; i < image->height; ++i) {
        for (uint64_t j = 0; j < image->width; ++j) {
            temp.data[image->height * j + image->height - i - 1] = image->data[image->width * i + j];
        }
    }
    return temp;
}
