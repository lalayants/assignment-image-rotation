//
// Created by Kirill Lalayants  on 10/10/22.
//

#ifndef TASK1_FLIPPER_H
#define TASK1_FLIPPER_H
#include "image.h"

struct image image_flip_anticlockwise_90(struct image * image);

#endif //TASK1_FLIPPER_H
