//
// Created by Kirill Lalayants on 10/9/22.
//

#ifndef TASK1_IMAGE_H
#define TASK1_IMAGE_H
#include <stdint.h>
#include <stdlib.h>

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image image_create(uint64_t w, uint64_t h);

void image_free(struct image * image);

#endif //TASK1_IMAGE_H
