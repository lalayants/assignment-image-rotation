//
// Created by Kirill Lalayants on 10/9/22.
//

#ifndef TASK1_FILE_OPENER_H
#define TASK1_FILE_OPENER_H
#include <stdio.h>

enum file_open_status {
    FILE_OPENED_UNSUCCESSFULLY = 0,
    FILE_OPENED_SUCCESSFULLY
};

enum file_close_status {
    FILE_CLOSED_UNSUCCESSFULLY = 0,
    FILE_CLOSED_SUCCESSFULLY
};

enum file_open_status open_file(char const *filename, char const *mode, FILE **f);

enum file_close_status close_file(FILE **f);

#endif //TASK1_FILE_OPENER_H
